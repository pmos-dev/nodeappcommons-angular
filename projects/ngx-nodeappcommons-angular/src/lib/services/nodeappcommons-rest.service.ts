import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { CommonsRestService } from 'ngx-httpcommons-rest';
import { CommonsSocketIoUrlOptions } from 'ngx-httpcommons-socket-io';

export abstract class NodeAppCommonsRestService extends CommonsRestService {
	constructor(
			http: HttpClient,
			configService: CommonsConfigService
	) {
		super(
				CommonsSocketIoUrlOptions.buildUrl(CommonsType.assertString(configService.getString('app', 'url'))),
				http
		);
	}
}
