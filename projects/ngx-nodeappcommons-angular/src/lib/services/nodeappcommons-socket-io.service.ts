import { CommonsType } from 'tscommons-core';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { CommonsSocketIoClientService } from 'ngx-httpcommons-socket-io';
import { CommonsSocketIoUrlOptions } from 'ngx-httpcommons-socket-io';

export abstract class NodeAppCommonsSocketIoService extends CommonsSocketIoClientService {
	constructor(
			configService: CommonsConfigService
	) {
		super(
				CommonsSocketIoUrlOptions.buildUrl(CommonsType.assertString(configService.getString('app', 'url'))),
				true,
				CommonsSocketIoUrlOptions.buildOptions(CommonsType.assertString(configService.getString('app', 'url')))
		);
	}
}
