import { Injectable } from '@angular/core';
import { EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { NodeAppCommonsSocketIoService } from './nodeappcommons-socket-io.service';

@Injectable({
	providedIn: 'root'
})
export class NodeAppCommonsSocketIoAppAngularService extends NodeAppCommonsSocketIoService {
	private onException: EventEmitter<string> = new EventEmitter<string>(true);
	private onCloseUi: EventEmitter<void> = new EventEmitter<void>(true);
	private onReload: EventEmitter<void> = new EventEmitter<void>(true);
	
	constructor(
			configService: CommonsConfigService
	) {
		super(configService);
	}
	
	protected setupOns(): void {
		this.on('app/exception', (message: string): void => {
			this.onException.emit(message);
		});
		
		this.on('ui/close', (): void => {
			this.onCloseUi.emit();
		});

		this.on('ui/reload', (): void => {
			this.onReload.emit();
		});
	}
	
	public exceptionObservable(): Observable<string> {
		return this.onException;
	}
	
	public closeUiObservable(): Observable<void> {
		return this.onCloseUi;
	}
	
	public reloadObservable(): Observable<void> {
		return this.onReload;
	}
}
