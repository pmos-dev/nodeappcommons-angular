import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ECommonsRunState } from 'tscommons-async';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { ECommonsContentType } from 'ngx-httpcommons-rest';

import { NodeAppCommonsRestService } from './nodeappcommons-rest.service';

@Injectable({
	providedIn: 'root'
})
export class NodeAppCommonsRestAppAngularService extends NodeAppCommonsRestService {
	constructor(
			http: HttpClient,
			configService: CommonsConfigService
	) {
		super(http, configService);
	}

	public async shutdown(): Promise<void> {
		await this.patch(
				'/app/run-state',
				{
					state: ECommonsRunState.COMPLETED
				},
				undefined,
				undefined,
				ECommonsContentType.JSON
		);
	}
}
