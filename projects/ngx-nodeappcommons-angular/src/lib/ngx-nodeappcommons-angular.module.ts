import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';
import { NgxAngularCommonsCacheModule } from 'ngx-angularcommons-cache';
import { NgxAngularCommonsAppModule } from 'ngx-angularcommons-app';

import { NgxHttpCommonsRestModule } from 'ngx-httpcommons-rest';
import { NgxHttpCommonsSocketIoModule } from 'ngx-httpcommons-socket-io';

import { NodeAppCommonsRestAppAngularService } from './services/nodeappcommons-rest-app-angular.service';
import { NodeAppCommonsSocketIoAppAngularService } from './services/nodeappcommons-socket-io-app-angular.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsCoreModule,
				NgxAngularCommonsCacheModule,
				NgxAngularCommonsAppModule,
				NgxHttpCommonsRestModule,
				NgxHttpCommonsSocketIoModule
		],
		declarations: [
		],
		exports: []
})
export class NgxNodeAppCommonsAngularModule {
	static forRoot(): ModuleWithProviders {
		return {
				ngModule: NgxNodeAppCommonsAngularModule,
				providers: [
						NodeAppCommonsRestAppAngularService,
						NodeAppCommonsSocketIoAppAngularService
				]
		};
	}
}
